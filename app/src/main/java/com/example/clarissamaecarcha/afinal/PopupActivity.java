package com.example.clarissamaecarcha.afinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.transition.Transition;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

/**
 * Created by Clarissa Mae Carcha on 07/09/2017.
 */

public class PopupActivity extends AppCompatActivity {
    FloatingActionButton fabClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);
        //target.fabClose = Utils.findRequiredViewAsType(source, R.id.fabClose, "field 'fabClose'", FloatingActionButton.class);


        fabClose = (FloatingActionButton)findViewById(R.id.fabClose) ;
        fabClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PopupActivity.this, MainActivity.class);

                startActivity(i);
                overridePendingTransition(R.anim.hold, R.anim.push_down_out);

            }
        });
    }



}
