package com.example.clarissamaecarcha.afinal;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton fabMiddle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fabMiddle = (FloatingActionButton)findViewById(R.id.fabMiddle) ;
        fabMiddle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, PopupActivity.class);

                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.hold);
            }
        });
    }
}
